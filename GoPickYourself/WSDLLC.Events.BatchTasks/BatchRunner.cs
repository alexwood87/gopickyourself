﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.BusinessLogic;
using WSDLLC.Events.Core.Contracts;
using System.Drawing;
namespace WSDLLC.Events.BatchTasks
{
    public class BatchRunner
    {
        private readonly IAppBusinessLogic _logic;
        public BatchRunner(string apiKey, string connString)
        {
            _logic = new AppBusinessLogic(connString);
            Luxand.FSDK.ActivateLibrary(apiKey);
            Luxand.FSDK.SetFaceDetectionParameters(true, true, 500);
        }
        public void RunImageChecker()
        {
            const int pageSize = 5000;
            long total;
        
            var pageIndex = 0;
            var users = _logic.SearchUsers(new Domain.Models.UserSearchModel
            {
                PageSize = int.MaxValue                
            }, out total);
            var evs = _logic.EventSearch(new Domain.Models.EventSearchModel
            {
                WantsImages = true,
                EventDateEnd = DateTime.UtcNow,
                EventDateStart = DateTime.UtcNow.AddHours(-12),
                PageSize = int.MaxValue
            });
            foreach(var ev in evs)
            {
                var imgs = ev.Images;
                if(imgs == null)
                {
                    continue;
                }
                foreach(var img in imgs)
                {
                    var tempImg = img.Picture;
                    
                    foreach (var u in users)
                    {
                        foreach(var im in u.ProfilePictures)
                        {
                            float accurracy = 0.75f;
                            var imPic = im.Picture;
                          
                            Luxand.FSDK.MatchFaces(ref imPic, ref tempImg, 
                                ref accurracy);
                            if(accurracy >= 0.75f)
                            {
                                _logic.AddImageToUser(img, u);
                            }
                        }
                    }
                }
                pageIndex++;
            }
           
        }
    }
}
