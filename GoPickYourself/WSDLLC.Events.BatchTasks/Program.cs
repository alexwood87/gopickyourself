﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.BatchTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            BatchRunner runner = new BatchRunner(ConfigurationManager.AppSettings["FaceSDKKey"],
                ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
            runner.RunImageChecker();
          
        }
    }
}
