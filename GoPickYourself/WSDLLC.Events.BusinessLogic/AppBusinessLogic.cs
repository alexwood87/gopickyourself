﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.App.Common;
using WSDLLC.Events.BusinessLogic.Helpers;
using WSDLLC.Events.Core.Contracts;
using WSDLLC.Events.Domain.Models;
using WSDLLC.Events.Domain.UnitsOfWork;
using WSDLLC.Events.Infrastructure.UnitsOfWork;

namespace WSDLLC.Events.BusinessLogic
{
    public class AppBusinessLogic : IAppBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public AppBusinessLogic(string connString)
        {
            _unitOfWork = new UnitOfWork(connString);
        }
        public void AddImageToUser(ImageModel imgModel, UserModel userModel)
        {
            userModel = InputCleaner.Clean(userModel, false);
            imgModel = InputCleaner.Clean(imgModel);
            _unitOfWork.UserRepository.AddImageToUser(imgModel, userModel);
        }

        public Guid AddOrUpdateEvent(SharedEventModel eventModel, string geoCodingApiKey)
        {
            eventModel = InputCleaner.Clean(eventModel);
            decimal lat, lon;
            if (eventModel.Address1 != null && eventModel.City != null && eventModel.StateOrProvince != null && eventModel.PostalCode != null)
            {
                CommonFunctions.GeoCode(eventModel.Address1, eventModel.Address2,
                    eventModel.City, eventModel.StateOrProvince, eventModel.PostalCode,
                    eventModel.Country, geoCodingApiKey, out lat, out lon);
                eventModel.Latitude = decimal.Parse(lat.ToString("G29"));
                eventModel.Longitude = decimal.Parse( lon.ToString("G29"));
            }
            return
                _unitOfWork.EventRepository.AddOrUpdateEvent(eventModel);
        }

        public Guid AddOrUpdateImage(ImageModel image, byte[] watermark)
        {
            image = InputCleaner.Clean(image);
            image.Picture = CommonFunctions.PaintImageOnImage(image.Picture, watermark, 0, 0, 25, 25);
            return
                _unitOfWork.ImageRepository.AddOrUpdateImage(image);
        }

        public void AddTagsToEvent(Guid eventId, List<string> tags)
        {
            _unitOfWork.EventRepository.AddTagsToEvent(eventId, tags);
        }

        public void AddTagsToImage(Guid imageId, List<string> tags)
        {
            _unitOfWork.ImageRepository.AddTagsToImage(imageId, tags);
        }

        public List<SharedEventModel> EventSearch(EventSearchModel search)
        {
            search = InputCleaner.Clean(search);
           
            return
                _unitOfWork.EventRepository.EventSearch(search);
        }

        public long RegisterOrUpdateUser(UserModel user)
        {

            if (user.UserId > 0)
            {
                long total = 0;
                var u = _unitOfWork.UserRepository.SearchUsers(new UserSearchModel
                {
                    UserId = user.UserId,
                    PageSize = 1
                }, out total).SingleOrDefault();
                if (u != null)
                {
                    if (string.IsNullOrEmpty(user.Email))
                    {
                        user.Email = u.Email;
                    }
                    if (string.IsNullOrEmpty(user.PhoneNumber))
                    {
                        user.PhoneNumber = u.PhoneNumber;
                    }
                    if (string.IsNullOrEmpty(user.Pin))
                    {
                        user.Pin = u.Pin;
                    }
                    if (string.IsNullOrEmpty(user.Username))
                    {
                        user.Username = u.Username;
                    }
                }
            }
            else
            {
                user = InputCleaner.Clean(user);
            }
            return
                _unitOfWork.UserRepository.RegisterOrUpdateUser(user);
        }

        public void RemoveTagFromEvent(Guid eventId, string tagName)
        {
            _unitOfWork.EventRepository.RemoveTagFromEvent(eventId, tagName);
        }

        public void RemoveTagFromImage(Guid image, string tagName)
        {
            _unitOfWork.ImageRepository.RemoveTagFromImage(image, tagName);
        }

        public List<ImageModel> SearchImages(ImageSearchModel search)
        {
            search = InputCleaner.Clean(search);
            return
                _unitOfWork.ImageRepository.SearchImages(search);
        }

        public List<UserModel> SearchUsers(UserSearchModel search, out long total)
        {
            search = InputCleaner.Clean(search);
            return
                _unitOfWork.UserRepository.SearchUsers(search, out total);
        }
        public List<ImageModel> GetAllImages(int pageSize, int pageIndex, out long total)
        {
            return
                _unitOfWork.ImageRepository.GetAllImages(pageSize, pageIndex, out total);
        }
        public UserModel ValidateUser(string phone, string username, string email, string pin)
        {
            return
                _unitOfWork.UserRepository.ValidateUser(phone, username, email, Encrypter.EncryptPassword(pin));
        }

        public void ValidateCode(string username, string code, string newPassword)
        {
            if(InputCleaner.IsValidEmail(username, false))
            {
                username = InputCleaner.ToLowerAndTrim(username);           
            }
            else
            {
                username = InputCleaner.NumbersOnly(username, true);              
            }
            _unitOfWork.UserRepository.ValidateCode(username, code, Encrypter.EncryptPassword(newPassword));
        }

        public void GenerateCode(string email, string phone, string usernameEmail,
            string passwordEmail, string accoundId, string smsProviderUrl)
        {
            if(!InputCleaner.IsValidEmail(email, false))
            {
                throw new ArgumentException("Email Is Invalid");
                //phone = InputCleaner.NumbersOnly(phone, true);
            }
            var code = Guid.NewGuid().ToString();

            _unitOfWork.UserRepository.GenerateCode(code, email, phone);
            if (InputCleaner.IsValidEmail(email, false))
            {
                CommonFunctions.SendEmail(usernameEmail, email, "Your reset code is: " + code,
                    "Go Pick Yourself - Reset Code", usernameEmail, passwordEmail);   
            }
            else
            {
                CommonFunctions.SendSMS(accoundId, usernameEmail, passwordEmail,
                    InputCleaner.NumbersOnly(phone, true),
                    "Your Reset Code For Go Pick Yourself Is: " + code,
                    smsProviderUrl);
            }
        }

        public List<AnouncementModel> SearchAnouncement(AnouncementSearch search)
        {
            return
                _unitOfWork.AnouncementRepository.SearchAnouncements(search);
        }

        public int AddOrUpdateAnouncement(AnouncementModel anouncement)
        {
            return
                _unitOfWork.AnouncementRepository.AddOrUpdateAnouncement(anouncement);
        }
        public void DeleteAnouncement(int id)
        {
            _unitOfWork.AnouncementRepository.Delete(id);
        }
    }
}
