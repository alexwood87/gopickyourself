﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Models;

namespace WSDLLC.Events.BusinessLogic.Helpers
{
    public class InputCleaner
    {
        public static string NumbersOnly(string str, bool isPhone = false)
        {
            if(string.IsNullOrEmpty(str))
            {
                return
                    null;
            }
            var ch = str.ToCharArray();
            var sb = new StringBuilder();
            foreach (var c in ch)
            {
                switch (c)
                {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        sb.Append(c);
                        break;
                }
                if (isPhone && c == '+')
                {
                    sb.Append(c);
                }
            }
            return
                sb.ToString();
        }
    
        public static bool IsValidEmail(string email, bool allowNull)
        {
            if (allowNull && email == null)
            {
                return true;
            }
            try
            {
                var m = new MailAddress(email);
                return
                    true;
            }
            catch (Exception ex)
            {
                return
                    false;
            }
        }
        public static ImageModel Clean(ImageModel img)
        {
            if (img.Tags != null)
            {
                var list = new List<TagModel>();
                foreach (var t in img.Tags)
                {
                    list.Add(new TagModel { TagName = ToLowerAndTrim(t.TagName) , TagId = t.TagId});
                }
            }
            return
                img;
        }
        public static SharedEventModel Clean(SharedEventModel ev)
        {
            if (ev.Coordinators != null)
            {
                for (int i = 0; i < ev.Coordinators.Count;i++)
                {
                    ev.Coordinators[i] = Clean(ev.Coordinators[i]);
                }
            }
            if(ev.Tags != null)
            {
                for (int i = 0; i < ev.Tags.Count; i++)
                {
                    var t = ev.Tags[i];

                    t.TagName = ToLowerAndTrim(t.TagName);
                    ev.Tags[i] = t;
                }
            }
            if(ev.Images != null)
            {
                for (int i = 0; i < ev.Images.Count; i++)
                {
                    ev.Images[i] = Clean(ev.Images[i]);
                }
            }
            if(ev.EventDate == DateTime.MinValue)
            {
                ev.EventDate = DateTime.UtcNow.AddDays(1);
            }
            if(ev.CreatedDate == DateTime.MinValue)
            {
                ev.CreatedDate = DateTime.UtcNow;
            }
            if(ev.Description != null)
            {
                ev.Description = ev.Description.Trim();
            }
            var len = ev.Name.Length > 99 ? 100 : ev.Name.Length;
            ev.Name = ev.Name.Substring(0,len).Trim();
            return
                ev;
        }
        public static EventSearchModel Clean(EventSearchModel search)
        {
            search.Description = ToLowerAndTrim(search.Description);
            search.EventName = ToLowerAndTrim(search.EventName);
            return search;
        } 
        public static ImageSearchModel Clean(ImageSearchModel search)
        {
            search.EventDescription = ToLowerAndTrim(search.EventDescription);
            search.EventName = ToLowerAndTrim(search.EventName);
            if(search.Tags != null)
            {
                for(var s =0; s < search.Tags.Count;s++)
                {
                    search.Tags[s] = ToLowerAndTrim(search.Tags[s]);
                }
            }
            return
                search;
        }
        public static UserSearchModel Clean(UserSearchModel search)
        {
            if(!IsValidEmail(search.Email, true))
            {
                throw new ArgumentException("Email Is Invalid");
            }
            search.FirstName = ToLowerAndTrim(search.FirstName);
            search.LastName = ToLowerAndTrim(search.LastName);
            search.NickName = ToLowerAndTrim(search.NickName);
            search.PhoneNumber = NumbersOnly(search.PhoneNumber, true);
            search.Username = ToLowerAndTrim(search.Username);
            if(search.Image != null)
            {
                search.Image = Clean(search.Image);                   
            }
            return
                search;
        }
        public static UserModel Clean(UserModel user, bool validateEmail = true)
        {
            if(validateEmail && !IsValidEmail(user.Email,true))
            {
                throw new ArgumentException("Email Is Invalid!");
            }
            user.Email = Encrypter.EncryptPassword(user.Email);
            user.FirstName = user.FirstName.Trim();
            user.LastName = user.LastName.Trim();
            user.PhoneNumber = Encrypter.EncryptPassword(NumbersOnly(user.PhoneNumber, true));
            user.Username = ToLowerAndTrim(user.Username);
            user.Pin = Encrypter.EncryptPassword(user.Pin);
            return
                user;
        }
        public static string ToLowerAndTrim(string str)
        {
            if(str ==  null)
            {
                return
                    null;
            }
            return
                str.ToLower().Trim();

        }
    }
}
