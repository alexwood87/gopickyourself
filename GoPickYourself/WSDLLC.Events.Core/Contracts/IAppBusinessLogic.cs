﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Models;

namespace WSDLLC.Events.Core.Contracts
{
    public interface IAppBusinessLogic
    {
        void DeleteAnouncement(int id);
        List<AnouncementModel> SearchAnouncement(AnouncementSearch search);
        int AddOrUpdateAnouncement(AnouncementModel anouncement);
        Guid AddOrUpdateEvent(SharedEventModel eventModel, string geoCodingApiKey);
        List<SharedEventModel> EventSearch(EventSearchModel search);
        void AddTagsToEvent(Guid eventId, List<string> tags);
        void RemoveTagFromEvent(Guid eventId, string tagName);
        Guid AddOrUpdateImage(ImageModel image, byte[] watermark);
        void AddTagsToImage(Guid imageId, List<string> tags);
        List<ImageModel> SearchImages(ImageSearchModel search);
        List<ImageModel> GetAllImages(int pageSize, int pageIndex, out long total);
        void RemoveTagFromImage(Guid image, string tagName);
        UserModel ValidateUser(string phone, string username, string email, string pin);
        List<UserModel> SearchUsers(UserSearchModel search, out long total);
        long RegisterOrUpdateUser(UserModel user);
        void AddImageToUser(ImageModel imgModel, UserModel userModel);
        void ValidateCode(string username, string code, string newPassword);
        void GenerateCode(string email, string phone, string usernameEmail, 
            string passwordEmail, string accoundId, string smsProviderUr);
    }
}
