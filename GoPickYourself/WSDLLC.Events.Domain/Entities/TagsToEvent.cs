//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WSDLLC.Events.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class TagsToEvent
    {
        public System.Guid TagsToEventsId { get; set; }
        public long TagId { get; set; }
        public System.Guid SharedEventId { get; set; }
    
        public virtual SharedEvent SharedEvent { get; set; }
        public virtual Tag Tag { get; set; }
    }
}
