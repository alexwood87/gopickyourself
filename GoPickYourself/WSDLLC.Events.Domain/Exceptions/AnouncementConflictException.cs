﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Exceptions
{
    public class AnouncementConflictException : Exception
    {
        public AnouncementConflictException(string message):base(message)
        {

        }
    }
}
