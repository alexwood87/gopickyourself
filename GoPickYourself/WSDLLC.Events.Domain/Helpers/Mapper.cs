﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Entities;
using WSDLLC.Events.Domain.Models;

namespace WSDLLC.Events.Domain.Helpers
{
    public class Mapper
    {
        public static ImageModel ImageToModel(Image img, bool deepCopy)
        {
            if (img == null)
            {
                return null;
            }
            return
                new ImageModel
                {
                    ImageId = img.ImageId,
                    IsProfilePicture = img.ImagesToUsers.Any(x => x.IsProfilePicture),
                    PeopleInPicture = deepCopy ? img.ImagesToUsers.Select(x => UserToUserModel(x.User)).ToList() : null,
                    Picture = img.Picture,
                    Code = img.Code,
                    Tags = img?.TagsToImages.Select(x => TagsToTagModel(x.Tag)).ToList()
                };
        }
        public static User UserModeltoUser(UserModel model)
        {
            if(model == null)
            {
                return
                    null;
            }
            return
                new User
                {
                    AuthorizationLevel = (int)model.UserLevel,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    NickName = model.NickName,
                    PhoneNumber = model.PhoneNumber,
                    Pin = model.Pin,
                    UserId = model.UserId,
                    Username = model.Username                    
                };
        }
        public static SharedEventModel EventsToEventModels(SharedEvent ev, bool deepCopy, bool wantTags = true)
        {
            if(ev == null)
            {
                return
                    null;
            }
            return
                new SharedEventModel
                {
                    CreatedDate = ev.CreatedDate,
                    Code = ev.Code,
                    EventEndDate = ev.EventEndDate,
                    Description = ev.Description,
                    EventDate = ev.EventDate,
                    EventId = ev.SharedEventId,
                    EventType = (EventTypeModel)ev.EventType.EventTypeId,
                    Name = ev.EventName,
                    Coordinators =ev.EventsToUsers.Select(x => UserToUserModel(x.User)).ToList(),
                    Tags = ev.TagsToEvents.Select(x => TagsToTagModel(x.Tag)).ToList(),
                    Images = !deepCopy ? ev.Images.Select(x => ImageToModel(x, false)).ToList() : null,
                    Address1 = ev.Address1,
                    Address2 = ev.Address2,
                    City = ev.City,
                    StateOrProvince = ev.StateOrProvince,
                    PostalCode = ev.PostalCode,
                    Country = ev.Country,
                    Latitude = ev.Latitude,
                    Longitude = ev.Longitude
                };
           
        }
        public static UserModel UserToUserModel(User u, bool deepCopy = false)
        {
            if(u == null)
            {
                return
                    null;
            }
            return
                new UserModel
                {
                    Email = u.Email,
                    PhoneNumber = u.PhoneNumber,
                    Pin = u.Pin,
                    ProfilePictures = deepCopy ? u.ImagesToUsers.Where(x => x.IsProfilePicture)
                        .Select(x => ImageToModel(x.Image, false)).ToList() : null,
                    UserId = u.UserId,
                    UserLevel = (UserLevelModel) u.AuthorizationLevel,
                    Username = u.Username,
                    NickName = u.NickName,
                    FirstName = u.FirstName,
                    LastName = u.LastName
                };
        }
        public static TagModel TagsToTagModel(Tag t)
        {
            if(t == null)
            {
                return null;
            }
            return
                new TagModel
                {
                    TagId = t.TagId,
                    TagName = t.TagName
                };
        }
        public static Tag TagModelToTag(TagModel t)
        {
            if(t == null)
            {
                return null;
            }
            return
                new Tag
                {
                    TagId = t.TagId,
                    TagName = t.TagName
                };
        }
        public static SharedEvent EventModelToEvent(SharedEventModel ev, bool deepCopy)
        {
            if(ev == null)
            {
                return
                    null;
            }
            return
                new SharedEvent
                {
                    Code = ev.Code,
                    CreatedDate = ev.CreatedDate,
                    Description = ev.Description,
                    EventDate = ev.EventDate,
                    EventEndDate = ev.EventEndDate,
                    EventName = ev.Name,
                    EventTypeId = (int)ev.EventType,
                    EventsToUsers = deepCopy ? ev.Coordinators.Select(x => new EventsToUser
                    {
                        IsCoordinator = true,
                        SharedEventId = ev.EventId,
                        UserId = x.UserId
                    }).ToList() : null,
                    Images = deepCopy ? ev.Images.Select(ImageModelsToImage).ToList() : null,
                    SharedEventId = ev.EventId,
                    TagsToEvents = deepCopy ? ev.Tags.Select(x => new TagsToEvent
                    {
                        SharedEventId = ev.EventId,
                        TagId = x.TagId
                    }).ToList() : null,
                    Address1 = ev.Address1,
                    Address2 = ev.Address2,
                    City = ev.City,
                    Country = ev.Country,
                    Latitude = ev.Latitude,
                    Longitude = ev.Longitude,
                    PostalCode = ev.PostalCode,
                    StateOrProvince = ev.StateOrProvince

                };
        }
        public static Image ImageModelsToImage(ImageModel img)
        {
            if(img == null)
            {
                return
                    null;
            }
            return
                new Image
                {
                    Code = img.Code,
                     ImageId = img.ImageId,
                     Picture = img.Picture,
                     SharedEventId = img.Event == null ? (Guid?)null : img.Event.EventId                     
                };
        }
    }
}
