﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Models
{
    [DataContract]
    public class AnouncementModel
    {
        [DataMember]
        public int AnouncementId { get; set; }
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
    }
}
