﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Models
{
    [DataContract]
    public class EventSearchModel
    {
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public Guid EventId { get; set; }
        [DataMember]
        public long? UserId { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public decimal? Latitude { get; set; }
        [DataMember]
        public decimal? Longitude { get; set; }
        [DataMember]
        public DateTime? EventDateStart { get; set; }
        [DataMember]
        public DateTime? EventDateEnd { get; set; }
        [DataMember]
        public string EventName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public bool WantsImages { get; set; }
        [DataMember]
        public List<string> Tags { get; set; } 
    }
}
