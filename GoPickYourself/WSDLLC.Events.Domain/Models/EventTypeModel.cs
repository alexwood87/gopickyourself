﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Models
{
    [DataContract]
    public enum EventTypeModel : int
    {
        [EnumMember]
        PUBLIC=1,
        [EnumMember]
        FRIENDSONLY,
        [EnumMember]
        PRIVATE
    }
}
