﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Models
{
    [DataContract]
    public class ImageModel
    {
        [DataMember]
        public Guid ImageId { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public bool IsProfilePicture { get; set; }
      
        [DataMember]
        public byte[] Picture { get; set; }
        [DataMember]
        public SharedEventModel Event { get; set; }
        [DataMember]
        public List<UserModel> PeopleInPicture { get; set; }
        [DataMember]
        public List<TagModel> Tags { get; set; }
    }
}
