﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Models
{
    [DataContract]
    public class ImageSearchModel
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public bool? IsProfilePic { get; set; }
        [DataMember]
        public Guid ImageId { get; set; }
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public Guid EventId { get; set; }
        [DataMember]
        public List<string> Tags { get; set; }
        [DataMember]
        public string EventName { get; set; }
        [DataMember]
        public string EventDescription { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
    }
}
