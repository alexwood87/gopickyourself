﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Models
{
    [DataContract]
    public class SharedEventModel
    {
        [DataMember]
        public List<UserModel> Coordinators { get; set; }
        [DataMember]
        public Guid EventId { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string StateOrProvince { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public decimal? Latitude { get; set; }
        [DataMember]
        public decimal? Longitude { get; set; }
        [DataMember]
        public EventTypeModel EventType { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime EventDate { get; set; }
        [DataMember]
        public DateTime? EventEndDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public List<ImageModel> Images { get; set; }
        [DataMember]
        public List<TagModel> Tags { get; set; }
    }
}
