﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Models
{
    [DataContract]
    public class TagModel
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public string TagName { get; set; } 
    }
}
