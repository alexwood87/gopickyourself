﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Models
{
    [DataContract]
    public enum UserLevelModel : int
    {
        SUPERADMIN = 0,
        ADMIN = 1,
        MODERATOR = 2,
        USER = 3
    }
}
