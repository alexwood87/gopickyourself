﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSDLLC.Events.Domain.Models
{
    [DataContract]
    public class UserModel
    {
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Pin { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string NickName { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public UserLevelModel UserLevel { get; set; }
        [DataMember]
        public List<ImageModel> ProfilePictures { get; set; }
    }
}
