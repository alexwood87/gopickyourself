﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Models;

namespace WSDLLC.Events.Domain.Repositories
{
    public interface IAnouncementRepository
    {
        List<AnouncementModel> SearchAnouncements(AnouncementSearch search);
        int AddOrUpdateAnouncement(AnouncementModel model);
        void Delete(int id);
    }
}
