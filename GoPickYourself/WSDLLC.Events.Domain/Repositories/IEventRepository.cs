﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Models;

namespace WSDLLC.Events.Domain.Repositories
{
    public interface IEventRepository
    {
        Guid AddOrUpdateEvent(SharedEventModel eventModel);
        List<SharedEventModel> EventSearch(EventSearchModel search);
        void AddTagsToEvent(Guid eventId, List<string> tags);
        void RemoveTagFromEvent(Guid eventId, string tagName);
    }
}
