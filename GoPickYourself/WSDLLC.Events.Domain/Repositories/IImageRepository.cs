﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Models;

namespace WSDLLC.Events.Domain.Repositories
{
    public interface IImageRepository
    {
        Guid AddOrUpdateImage(ImageModel image);
        void AddTagsToImage(Guid imageId, List<string> tags);
        List<ImageModel> SearchImages(ImageSearchModel search);
        void RemoveTagFromImage(Guid image, string tagName);
        List<ImageModel> GetAllImages(int pageSize, int pageIndex, out long total);
    }
}
