﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Models;

namespace WSDLLC.Events.Domain.Repositories
{
    public interface IUserRepository
    {
        UserModel ValidateUser(string phone, string username, string email, string pin);
        List<UserModel> SearchUsers(UserSearchModel search, out long total);
        long RegisterOrUpdateUser(UserModel user);
        void AddImageToUser(ImageModel imgModel, UserModel userModel);
        void ValidateCode(string username, string code, string newPassword);
        void GenerateCode(string code, string email, string phone);
    }
}
