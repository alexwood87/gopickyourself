﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Repositories;

namespace WSDLLC.Events.Domain.UnitsOfWork
{
    public interface IUnitOfWork
    {
        IUserRepository UserRepository { get; set; }
        IImageRepository ImageRepository { get; set; }
        IEventRepository EventRepository { get; set; }
        IAnouncementRepository AnouncementRepository { get; set; }
    }
}
