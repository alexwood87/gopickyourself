﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Entities;
using WSDLLC.Events.Domain.Exceptions;
using WSDLLC.Events.Domain.Models;
using WSDLLC.Events.Domain.Repositories;

namespace WSDLLC.Events.Infrastructure.Repositories
{
    public class AnouncementRepository : IAnouncementRepository
    {
        private readonly GoPickYourselfEntities _context;
        public AnouncementRepository(GoPickYourselfEntities context)
        {
            _context = context;
        }
        public int AddOrUpdateAnouncement(AnouncementModel model)
        {
            if(model.AnouncementId > 0)
            {
                var mo = _context.Anouncements.Single(x => x.AnouncementId == model.AnouncementId);
                mo.Anouncement1 = model.Text;
                mo.StartDate = model.StartDate;
                mo.EndDate = model.EndDate;
                _context.SaveChanges();
                return mo.AnouncementId;
            }
            var anouncement = _context.Anouncements.SingleOrDefault(x => x.EndDate >= model.EndDate && x.StartDate <= model.StartDate);
            if(anouncement != null)
            {
                throw new AnouncementConflictException("The Anouncement Must Be In A New Time Frame!");
            }
            anouncement = new Anouncement
            {
                Anouncement1 = model.Text,
                EndDate = model.EndDate,
                StartDate = model.StartDate
            };
            _context.Anouncements.Add(anouncement);
            _context.SaveChanges();
            return
                anouncement.AnouncementId;
        }

        public List<AnouncementModel> SearchAnouncements(AnouncementSearch search)
        {
            var query = _context.Anouncements.AsQueryable();
            if(search.Id != null)
            {
                query = query.Where(x => x.AnouncementId == search.Id.Value);
            }
            if(search.StartDate != null)
            {
                query = query.Where(x => x.StartDate >= search.StartDate.Value);
            }
            if(search.EndDate != null)
            {
                query = query.Where(x => x.EndDate <= search.EndDate.Value);
            }
            if(search.CurrentDate != null)
            {
                query = query.Where(x => x.EndDate >= search.CurrentDate.Value
                    && x.StartDate <= search.CurrentDate.Value);
            }
            return
                query.OrderBy(x => x.StartDate).ThenBy(x => x.EndDate)
                    .Skip(search.PageIndex * search.PageSize).Take(search.PageSize).ToList()
                    .Select(x => new AnouncementModel
                    {
                        AnouncementId = x.AnouncementId,
                        Text = x.Anouncement1,
                        EndDate = x.EndDate,
                        StartDate = x.StartDate
                    }).ToList();
        }
        public void Delete(int id)
        {
            var anouncement = _context.Anouncements.SingleOrDefault(x => x.AnouncementId == id);
            if (anouncement != null)
            {
                _context.Anouncements.Remove(anouncement);
                _context.SaveChanges();
            }
        }
    }
}
