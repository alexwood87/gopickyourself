﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Models;
using WSDLLC.Events.Domain.Repositories;
using WSDLLC.Events.Domain.Entities;
using WSDLLC.Events.Domain.Helpers;

namespace WSDLLC.Events.Infrastructure.Repositories
{
    public class EventRepository : IEventRepository
    {
        private readonly GoPickYourselfEntities _context;
        public EventRepository(GoPickYourselfEntities context)
        {
            _context = context;
        }
        public Guid AddOrUpdateEvent(SharedEventModel eventModel)
        {
            var ev = _context.SharedEvents.SingleOrDefault(x => x.SharedEventId == eventModel.EventId);

            if (ev == null || eventModel.EventId == Guid.Empty)
            {
                var m = Mapper.EventModelToEvent(eventModel, false);
                m.SharedEventId = Guid.NewGuid();
                _context.SharedEvents.Add(m);
                _context.SaveChanges();
                if(eventModel.Images != null)
                { 

                    foreach(var img in eventModel.Images)
                    {
                        img.Event = new SharedEventModel
                        {
                            EventId = m.SharedEventId
                        };
                        var im = Mapper.ImageModelsToImage(img);
                        im.ImageId = Guid.NewGuid();
                        _context.Images.Add(im);
                        _context.SaveChanges();
                       
                    }
                }
                if(eventModel.Tags != null)
                {
                    AddTagsToEvent(m.SharedEventId, eventModel.Tags.Select(x => x.TagName).ToList());
                }
                if(eventModel.Coordinators != null)
                {
                    foreach(var c in eventModel.Coordinators)
                    {
                        var etou = _context.EventsToUsers.SingleOrDefault(x => x.UserId == c.UserId && x.SharedEventId == m.SharedEventId);
                        if (etou == null)
                        {
                            etou = new EventsToUser
                            {
                                IsCoordinator = true,
                                SharedEventId = m.SharedEventId,
                                UserId = c.UserId,
                                EventToUsersId = Guid.NewGuid()
                            };

                            _context.EventsToUsers.Add(etou);
                            _context.SaveChanges();
                        }
                    }
                }
                return
                    m.SharedEventId;
            }
            ev.Code = eventModel.Code;
            ev.EventDate = eventModel.EventDate;
            ev.EventName = eventModel.Name;
            ev.Description = eventModel.Description;
            ev.Address1 = eventModel.Address1;
            ev.Address2 = eventModel.Address2;
            ev.City = eventModel.City;
            ev.Country = eventModel.Country;
            ev.Latitude = eventModel.Latitude;
            ev.Longitude = eventModel.Longitude;
            ev.StateOrProvince = eventModel.StateOrProvince;
            ev.PostalCode = eventModel.PostalCode;
            ev.EventEndDate = eventModel.EventEndDate;
            _context.SaveChanges();
            if (eventModel.Images != null)
            {
                foreach (var img in eventModel.Images)
                {
                    img.Event = new SharedEventModel
                    {
                        EventId = ev.SharedEventId
                    };
                    var im = Mapper.ImageModelsToImage(img);
                    if(im.ImageId == Guid.Empty)
                    {
                        im.ImageId = Guid.NewGuid();
                        _context.Images.Add(im);
                        _context.SaveChanges();
                    }
                   

                }
            }
            if (eventModel.Tags != null)
            {
                AddTagsToEvent(ev.SharedEventId, eventModel.Tags.Select(x => x.TagName).ToList());
            }

            if (eventModel.Coordinators != null)
            {
                foreach (var c in eventModel.Coordinators)
                {
                    var etou = _context.EventsToUsers
                        .SingleOrDefault(x => x.UserId == c.UserId && x.SharedEventId == ev.SharedEventId);
                    if (etou == null)
                    {
                        etou = new EventsToUser
                        {
                            IsCoordinator = true,
                            SharedEventId = ev.SharedEventId,
                            UserId = c.UserId,
                            EventToUsersId = Guid.NewGuid()                            
                        };

                        _context.EventsToUsers.Add(etou);
                        _context.SaveChanges();
                    }
                }
            }
            return
                ev.SharedEventId;
        }

        public void AddTagsToEvent(Guid eventId, List<string> tags)
        {
            var ev = _context.SharedEvents.SingleOrDefault(x => x.SharedEventId == eventId);
            if(ev == null)
            {
                return;
            }
            if(tags == null)
            {
                return;
            }
            foreach(var t in tags)
            {
                if(string.IsNullOrEmpty(t))
                {
                    continue;
                }
                var t2 = t.Trim().ToLower();
                if (string.IsNullOrEmpty(t2))
                {
                    continue;
                }
                var ta = _context.Tags.SingleOrDefault(x => x.TagName.ToLower().Trim() == t2);
                if(ta == null )
                {
                    ta = new Tag
                    {
                        TagName = t2
                    };
                    _context.Tags.Add(ta);
                    _context.SaveChanges();
                }
                
                var toe = _context.TagsToEvents.SingleOrDefault(x => x.TagId == ta.TagId && x.SharedEventId == eventId);
                if(toe == null)
                {
                    toe = new TagsToEvent
                    {
                        SharedEventId = eventId,
                        TagId = ta.TagId,
                        TagsToEventsId = Guid.NewGuid()                        
                    };
                    _context.TagsToEvents.Add(toe);
                    _context.SaveChanges();
                }
            }
        }

        public List<SharedEventModel> EventSearch(EventSearchModel search)
        {
            var query = _context.SharedEvents.AsQueryable();
            if (!string.IsNullOrEmpty(search.Code))
            {
                query = query.Where(x => x.Code == search.Code);
            }
            if(search.EventDateStart != null)
            {
                query = query.Where(x => x.EventDate >= search.EventDateStart.Value);
            }
            if(search.UserId != null)
            {
                query = query.Where(x => x.EventsToUsers.Any(y => y.UserId == search.UserId.Value));
            }
            if(search.EventDateEnd != null)
            {
                query = query.Where(x => x.EventEndDate != null 
                    && x.EventEndDate.Value <= search.EventDateEnd.Value);
            }
            if (!string.IsNullOrEmpty(search.EventName))
            {
                query = query.Where(x => x.EventName.ToLower().Trim().Contains(search.EventName.Trim().ToLower()));
            }
            if (!string.IsNullOrEmpty(search.Description))
            {
                query = query.Where(x => x.Description != null && x.Description.ToLower().Trim().Contains(search.Description.ToLower().Trim()));
            }
            if(search.EventId != Guid.Empty && search.EventId != null)
            {
                query = query.Where(x => x.SharedEventId == search.EventId);
            }
            if(search.Latitude != null)
            {
                query = query.Where(x => x.Latitude - 5 > search.Latitude.Value && x.Latitude + 5 < search.Latitude.Value);
            }
            if (search.Longitude != null)
            {
                query = query.Where(x => x.Longitude - 5 > search.Longitude.Value 
                && x.Longitude + 5 < search.Longitude.Value);
            }
            if(!string.IsNullOrEmpty(search.City))
            {
                query = query.Where(x => x.City.Trim().ToLower().Contains( search.City.ToLower().Trim()));
            }
            if (search.Tags != null)
            {
                foreach(var t in search.Tags)
                {
                    query = query.Where(x => x.TagsToEvents.Any(y => y.Tag.TagName.ToLower().Trim() == t.Trim().ToLower()));
                }
            }
            return
                query.OrderByDescending(z => z.EventDate).Skip(search.PageSize * search.PageIndex)
                    .Take(search.PageSize).ToList().
                        Select(x => Mapper.EventsToEventModels(x, search.WantsImages, false)).ToList();
        }

        public void RemoveTagFromEvent(Guid eventId, string tagName)
        {
            var tags = _context.SharedEvents.Single(x => x.SharedEventId == eventId);
            var ts = (tags.TagsToEvents.SingleOrDefault(x => x.Tag.TagName == tagName));
            if(ts != null)
            {
                var t = _context.TagsToEvents.Single(x => x.TagsToEventsId == ts.TagsToEventsId);
                _context.TagsToEvents.Remove(t);
                _context.SaveChanges();
            }   
            
        }
    }
}
