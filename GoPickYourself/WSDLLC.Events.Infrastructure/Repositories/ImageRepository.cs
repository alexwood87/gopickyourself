﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Entities;
using WSDLLC.Events.Domain.Helpers;
using WSDLLC.Events.Domain.Models;
using WSDLLC.Events.Domain.Repositories;

namespace WSDLLC.Events.Infrastructure.Repositories
{
    public class ImageRepository : IImageRepository
    {
        private readonly GoPickYourselfEntities _context;
        public ImageRepository(GoPickYourselfEntities context)
        {
            _context = context;
        }
        public Guid AddOrUpdateImage(ImageModel image)
        {
            if(image.ImageId == null || image.ImageId == Guid.Empty)
            {
                var img2 = Mapper.ImageModelsToImage(image);
                img2.ImageId = Guid.NewGuid();
                _context.Images.Add(img2);
                _context.SaveChanges();
                if(image.Tags != null)
                {
                    foreach(var t2 in image.Tags)
                    {
                        var ta2 = _context.Tags.SingleOrDefault(x => x.TagName == t2.TagName);
                        if (ta2 == null)
                        {
                            ta2 = new Tag
                            {
                                TagName = t2.TagName
                            };
                            _context.Tags.Add(ta2);
                            _context.SaveChanges();
                            var imgsToTags = new TagsToImage
                            {
                                TagsToImagesId = Guid.NewGuid(),
                                ImageId = img2.ImageId,
                                TagId = ta2.TagId
                            };
                            _context.TagsToImages.Add(imgsToTags);
                            _context.SaveChanges();
                            continue;
                        }
                        var img2Tag = _context.TagsToImages.SingleOrDefault(x => x.TagId == ta2.TagId && x.ImageId == image.ImageId);
                        if (img2Tag == null)
                        {
                            var imgsToTags = new TagsToImage
                            {
                                ImageId = img2.ImageId,
                                TagId = ta2.TagId,
                                TagsToImagesId = Guid.NewGuid()
                            };
                            _context.TagsToImages.Add(imgsToTags);
                            _context.SaveChanges();

                        }
                    }
                }
                return
                    img2.ImageId;
            }
            var img = _context.Images.SingleOrDefault(x => x.ImageId == image.ImageId);
            img.Picture = image.Picture;
            img.SharedEventId = image.Event.EventId;
            img.Code = image.Code;
            _context.SaveChanges();
            if (image.Tags != null)
            {
                foreach (var t in image.Tags)
                {
                    var ta = _context.Tags.SingleOrDefault(x => x.TagName == t.TagName);
                    if (ta == null)
                    {
                        ta = new Tag
                        {
                            TagName = t.TagName
                        };
                        _context.Tags.Add(ta);
                        _context.SaveChanges();
                        var imgsToTags = new TagsToImage
                        {
                            ImageId = img.ImageId,
                            TagId = ta.TagId,
                            TagsToImagesId = Guid.NewGuid()
                        };
                        _context.TagsToImages.Add(imgsToTags);
                        _context.SaveChanges();
                        continue;
                    }
                    var img2Tag = _context.TagsToImages.SingleOrDefault(x => x.TagId == ta.TagId && x.ImageId == image.ImageId);
                    if (img2Tag == null)
                    {
                        var imgsToTags = new TagsToImage
                        {
                            ImageId = img.ImageId,
                            TagId = ta.TagId,
                            TagsToImagesId = Guid.NewGuid()
                        };
                        _context.TagsToImages.Add(imgsToTags);
                        _context.SaveChanges();

                    }
                }
            }
            return
                img.ImageId;
        }

        public void AddTagsToImage(Guid imageId, List<string> tags)
        {
            if (tags == null)
                return;
            var img = _context.Images.SingleOrDefault(x => x.ImageId == imageId);
            if(img == null)
            {
                return;
            }
            foreach (var t in tags)
            {
                var ta = _context.Tags.SingleOrDefault(x => x.TagName == t);
                if (ta == null)
                {
                    ta = new Tag
                    {
                        TagName = t
                    };
                    _context.Tags.Add(ta);
                    _context.SaveChanges();
                    var imgsToTags = new TagsToImage
                    {
                        ImageId = img.ImageId,
                        TagId = ta.TagId,
                        TagsToImagesId = Guid.NewGuid()
                    };
                    _context.TagsToImages.Add(imgsToTags);
                    _context.SaveChanges();
                    continue;
                }
                var img2Tag = _context.TagsToImages.SingleOrDefault(x => x.TagId == ta.TagId && x.ImageId == img.ImageId);
                if (img2Tag == null)
                {
                    var imgsToTags = new TagsToImage
                    {
                        ImageId = img.ImageId,
                        TagId = ta.TagId,
                        TagsToImagesId = Guid.NewGuid()
                    };
                    _context.TagsToImages.Add(imgsToTags);
                    _context.SaveChanges();

                }
            }
            
        }

        public void RemoveTagFromImage(Guid image, string tagName)
        {
            var img = _context.Images.SingleOrDefault(x => x.ImageId == image);
            if(img == null)
            {
                return;
            }
            var t = img.TagsToImages.SingleOrDefault(x => x.Tag.TagName == tagName);
            if (t != null)
            {
                _context.TagsToImages.Remove(t);
                _context.SaveChanges();
            }
        }

        public List<ImageModel> SearchImages(ImageSearchModel search)
        {
            var images = _context.Images.AsQueryable();
            if(search.ImageId != null && search.ImageId != Guid.Empty)
            {
                images = images.Where(x => x.ImageId == search.ImageId);
            }
            if(!string.IsNullOrEmpty(search.Code))
            {
                images = images.Where(x => x.Code == search.Code);
            }
            if(search.IsProfilePic != null)
            {
                images = images.Where(x => x.ImagesToUsers.Any(y => y.IsProfilePicture == search.IsProfilePic.Value));
            }
            if(search.EventId != null && search.EventId != Guid.Empty)
            {
                images = images.Where(x => x.SharedEventId == search.EventId);
            }
            if(!string.IsNullOrEmpty(search.EventName))
            {
                images = images.Where(x => x.SharedEvent.EventName.ToLower().Contains(search.EventName.ToLower()));
            }
            if(!string.IsNullOrEmpty(search.EventDescription))
            {
                images = images.Where(x => x.SharedEvent.Description != null && x.SharedEvent.Description.ToLower().Contains(search.EventDescription.ToLower()));
            }
            if(search.UserId >0)
            {
                images = images.Where(x => x.ImagesToUsers.Any(y => y.UserId == search.UserId));
            }
            if(search.Tags != null)
            {
                foreach(var t in search.Tags)
                {
                    images = images.Where(x => x.TagsToImages.Any(y => y.Tag.TagName.ToLower() == t.ToLower()));
                }
            }
            return
                images.OrderByDescending(x => x.ImageId).Skip(search.PageIndex * search.PageSize)
                    .Take(search.PageSize).ToList().Select(x => Mapper.ImageToModel(x, false)).ToList();
        }
        public List<ImageModel> GetAllImages(int pageSize, int pageIndex, out long total)
        {
            total = _context.Images.LongCount() / pageSize;
            return
                _context.Images.OrderBy(x => x.ImageId).Skip(pageIndex * pageSize)
                    .Take(pageSize).ToList().Select(x => Mapper.ImageToModel(x, false)).ToList();
        }
    }
}
