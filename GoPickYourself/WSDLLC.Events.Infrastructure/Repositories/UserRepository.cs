﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Entities;
using WSDLLC.Events.Domain.Helpers;
using WSDLLC.Events.Domain.Models;
using WSDLLC.Events.Domain.Repositories;

namespace WSDLLC.Events.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly GoPickYourselfEntities _context;
        public UserRepository(GoPickYourselfEntities context)
        {
            _context = context;
        }
        public long RegisterOrUpdateUser(UserModel user)
        {
            if(user.UserId == 0)
            {
                var userE = _context.Users.SingleOrDefault(x => x.Email == user.Email ||
                x.PhoneNumber == user.PhoneNumber || x.Username == user.Username);
                if(userE == null)
                {
                    userE = Mapper.UserModeltoUser(user);
                    _context.Users.Add(userE);
                    _context.SaveChanges();
                    return userE.UserId;
                }
                userE.Username = user.Username;
                userE.LastName = user.LastName;
                userE.FirstName = user.FirstName;
                userE.Email = user.Email;
                userE.PhoneNumber = user.PhoneNumber;
                userE.Pin = user.Pin;
                _context.SaveChanges();
                return
                    userE.UserId;
            }
            var u = _context.Users.Single(x => x.UserId == user.UserId);
            u.Username = user.Username;
            u.LastName = user.LastName;
            u.FirstName = user.FirstName;
            u.Email = user.Email;
            u.PhoneNumber = user.PhoneNumber;
            u.Pin = user.Pin;
            u.AuthorizationLevel =(int) user.UserLevel;
            _context.SaveChanges();
            return
                u.UserId;
        }

        public List<UserModel> SearchUsers(UserSearchModel search, out long total)
        {
            var users = _context.Users.AsQueryable();
            if(!string.IsNullOrEmpty(search.Email))
            {
                users = users.Where(x => x.Email.ToLower().Contains(search.Email.ToLower()));
            }
            if(!string.IsNullOrEmpty(search.PhoneNumber))
            {
                users = users.Where(x => x.PhoneNumber.Contains(search.PhoneNumber));
            }
            if(!string.IsNullOrEmpty(search.Username))
            {
                users = users.Where(x => x.Username.ToLower().Contains(search.Username.ToLower()));
            }
            if(!string.IsNullOrEmpty(search.NickName))
            {
                users = users.Where(x => x.NickName.ToLower().Contains(search.NickName));
            }
            if(!string.IsNullOrEmpty(search.LastName))
            {
                users = users.Where(x => x.LastName.ToLower().Contains(search.LastName));
            }
            if(!string.IsNullOrEmpty(search.FirstName))
            {
                users = users.Where(x => x.FirstName.ToLower().Contains(search.FirstName));
            }
            if(search.UserId != null)
            {
                users = users.Where(x => x.UserId == search.UserId.Value);
            }
            if(search.EventId != null)
            {
                users = users.Where(x => x.EventsToUsers.Any(y => y.SharedEventId == search.EventId.Value));
            }
            if(search.Image != null && search.Image.ImageId != null && search.Image.ImageId != Guid.Empty)
            {
                users = users.Where(x => x.ImagesToUsers.Any(y => y.ImageId == search.Image.ImageId));
            }
            total = users.LongCount();
            return
                users.OrderBy(x => x.LastName).OrderBy(x => x.FirstName)
                    .Skip(search.PageIndex * search.PageSize)
                        .Take(search.PageSize).ToList().Select(x => Mapper.UserToUserModel(x, false)).ToList();
        }
        public void AddImageToUser(ImageModel imgModel, UserModel userModel)
        {
            var u = _context.Users.AsQueryable();
            User u2 = null;
            if(userModel.UserId > 0)
            {
                u2 = u.SingleOrDefault(x => x.UserId == userModel.UserId);
            }
            else if (!string.IsNullOrEmpty(userModel.Username))
            {
                u2 = u.SingleOrDefault(x => x.Username.Trim().ToLower() 
                    == userModel.Username.Trim().ToLower());
            }
           
            if(u2 == null)
            {
                return;
            }
            Guid imgId = imgModel.ImageId;
            if(imgModel.ImageId == Guid.Empty || imgModel.ImageId == null)
            {
                var repo = new ImageRepository(_context);
                imgId = repo.AddOrUpdateImage(imgModel);
            }

            var ue = _context.ImagesToUsers.SingleOrDefault(x => x.ImageId == imgId && x.UserId == u2.UserId);
            if(ue != null)
            {
                return;
            }
            ue = new ImagesToUser
            {
                UserId = u2.UserId,
                ImageId = imgId,
                IsProfilePicture = imgModel.IsProfilePicture,
                ImagesToUsersId = Guid.NewGuid()
            };
            _context.ImagesToUsers.Add(ue);
            _context.SaveChanges();
        }
        public UserModel ValidateUser(string phone, string username, string email, string pin)
        {
            var userE = _context.Users.SingleOrDefault(x => (x.Email == email ||
               x.PhoneNumber == phone || x.Username == username) && x.Pin == pin);
            return
               Mapper.UserToUserModel(userE, true);
        }

        public void ValidateCode(string username, string code, string newPassword)
        {
            var dt = DateTime.UtcNow.AddHours(-12);
            var codeE = _context.CodeResets.SingleOrDefault(x => x.Code == code && x.DateCreated > dt);
            if(codeE == null)
            {
                throw new ArgumentException("Code is invalid!");
            }
            
            if(codeE.User.Email.Trim().ToLower() == username || 
                codeE.User.Username.ToLower().Trim() == username || 
                codeE.User.PhoneNumber == username)
            {
                var u = _context.Users.Single(x => x.UserId == codeE.UserId);
                u.Pin = newPassword;
                _context.SaveChanges();
            }
        }
        public void GenerateCode(string code, string email, string phone)
        {
            if(string.IsNullOrEmpty(email))
            {
                var u = _context.Users.Single(x => x.PhoneNumber == phone);
                var codeR = new CodeReset
                {
                    Code = code,
                    DateCreated = DateTime.UtcNow,
                    UserId = u.UserId
                };
                _context.CodeResets.Add(codeR);
                _context.SaveChanges();
            }
            else
            {
                var u = _context.Users.Single(x => x.Email == email);
                var codeR = new CodeReset
                {
                    Code = code,
                    DateCreated = DateTime.UtcNow,
                    UserId = u.UserId
                };
                _context.CodeResets.Add(codeR);
                _context.SaveChanges();

            }
        }
    }
}
