﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.Domain.Entities;
using WSDLLC.Events.Domain.Repositories;
using WSDLLC.Events.Domain.UnitsOfWork;
using WSDLLC.Events.Infrastructure.Repositories;

namespace WSDLLC.Events.Infrastructure.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GoPickYourselfEntities _context;
        private IEventRepository _eventRepository;
        private IUserRepository _userRepository;
        private IImageRepository _imageRepository;
        private IAnouncementRepository _anouncementRepository;
        public UnitOfWork(string connString)
        {
            _context = new GoPickYourselfEntities(connString);
        }
        public IEventRepository EventRepository
        {
            get
            {
                return _eventRepository ?? (_eventRepository = new EventRepository(_context));
            }

            set
            {
                _eventRepository = value;
            }
        }

        public IImageRepository ImageRepository
        {
            get
            {
                return
                    _imageRepository ??(_imageRepository = new ImageRepository(_context));
            }

            set
            {
                _imageRepository = value;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                 return
                    _userRepository ?? (_userRepository = new UserRepository(_context));
            }

            set
            {
                _userRepository = value;
            }
        }

        public IAnouncementRepository AnouncementRepository
        {
            get
            {
                return
                    _anouncementRepository ?? (_anouncementRepository = new AnouncementRepository(_context));
            }
            set
            {
                _anouncementRepository = value;
            }
        }
    }
}
