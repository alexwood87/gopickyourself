﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.Events.BusinessLogic;
using WSDLLC.Events.Core.Contracts;
using WSDLLC.Events.Domain.Models;

namespace WSDLLC.Events.WebApp.Controllers
{
    public class AppApiController : Controller
    {
        private readonly IAppBusinessLogic _logic;
        public AppApiController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        // GET: AppApi
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddOrUpdateEvent(SharedEventModel eventModel)
        {
            try
            {
                var res = _logic.AddOrUpdateEvent(eventModel, ConfigurationManager.AppSettings["GoogleMapsApiKey"]);
                return
                    Json(new
                    {
                       Success = true,
                       Data = res 
                    }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EventSearch(EventSearchModel search)
        {
            try
            {
                var res = _logic.EventSearch(search);
                return
                    Json(new
                    {
                        Success = true,
                        Data = res
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddTagsToEvent(Guid eventId, List<string> tags)
        {
            try
            {
                _logic.AddTagsToEvent(eventId, tags);
                return
                    Json(new
                    {
                        Success = true
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult RemoveTagFromEvent(Guid eventId, string tagName)
        {
            try
            {
                _logic.RemoveTagFromEvent(eventId, tagName);
                return
                    Json(new
                    {
                        Success = true
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddOrUpdateImage(ImageModel image)
        {
            try
            {
                var res = _logic.AddOrUpdateImage(image, System.IO.File.ReadAllBytes(Server.MapPath("~/Content/GoPickYourselfLogo25.png")));
                return
                    Json(new
                    {
                        Success = true,
                        Data = res
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddTagsToImage(Guid imageId, List<string> tags)
        {
            try
            {
                _logic.AddTagsToImage(imageId, tags);
                return
                    Json(new
                    {
                        Success = true
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SearchImages(ImageSearchModel search)
        {
            try
            {
                var res = _logic.SearchImages(search);
                return
                    Json(new
                    {
                        Success = true,
                        Data = res
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllImages(int pageSize, int pageIndex, out long total)
        {
            try
            {
                var res = _logic.GetAllImages(pageSize,pageIndex, out total);
                return
                    Json(new
                    {
                        Success = true,
                        Data = res
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                total = 0;
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult RemoveTagFromImage(Guid imageId, string tagName)
        {
            try
            {
                _logic.RemoveTagFromImage(imageId, tagName);
                return
                    Json(new
                    {
                        Success = true
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ValidateUser(string phone, string username, string email, string pin)
        {
            try
            {
                var res = _logic.ValidateUser(phone,username,email,pin);
                return
                    Json(new
                    {
                        Success = true,
                        Data = res
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SearchUsers(UserSearchModel search)
        {
            try
            {
                long total;
                var res = _logic.SearchUsers(search, out total);
                return
                    Json(new
                    {
                        Success = true,
                        Data = res,
                        Total= total
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult RegisterOrUpdateUser(UserModel user, string picture64, string imageCode)
        {
            try
            {
                user.UserId = _logic.RegisterOrUpdateUser(user);
                if (picture64 != null)
                {
                    var pic = Convert.FromBase64String(picture64);
                    if (user.ProfilePictures == null)
                    {
                        user.ProfilePictures = new List<ImageModel>();

                    }
                    user.ProfilePictures.Add(new ImageModel
                    {
                        Code = imageCode,
                        Event = null,
                        IsProfilePicture = true,
                        PeopleInPicture = new List<UserModel>
                     {
                         user
                     },
                        Picture = pic
                    });
                    _logic.AddImageToUser(user.ProfilePictures[user.ProfilePictures.Count - 1], user);
                }
                return
                    Json(new
                    {
                        Data = user.UserId,
                        Success = true
                    }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new
                    {
                        Success = false,
                        Error = ex.ToString()
                    }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddImageToUser(ImageModel imgModel, UserModel userModel)
        {
            try
            {
                _logic.AddImageToUser(imgModel, userModel);
                return
                    Json(new
                    {
                        Success = true
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}