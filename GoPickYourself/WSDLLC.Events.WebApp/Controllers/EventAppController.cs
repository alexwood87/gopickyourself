﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WSDLLC.Events.BusinessLogic;
using WSDLLC.Events.Core.Contracts;
using WSDLLC.Events.Domain.Models;
using WSDLLC.Events.WebApp.Helpers;

namespace WSDLLC.Events.WebApp.Controllers
{
    [SecureContextHeader]
    [AuthorizationNeededHeader]
    public class EventAppController : Controller
    {
        private readonly IAppBusinessLogic _logic;
        public EventAppController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        public ActionResult Chat()
        {
            return View();
        }
        
        // GET: EventApp
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult CreateEvent()
        {
            return
                View();
        }
        [HttpGet]
        public ActionResult MyEvents()
        {
            return
                View(_logic.EventSearch(new EventSearchModel
                {
                    PageSize = int.MaxValue,
                    UserId = ((UserModel)Session["User"]).UserId
                }));
        }
       
        [HttpGet]
        public ActionResult EditEvent(Guid id)
        {
            return
                View(_logic.EventSearch(new EventSearchModel
                {
                    EventId = id,
                    UserId = (((UserModel)Session["User"]).UserId),
                    PageSize = 1
                }).Single());
        }
        [HttpGet]
        public ActionResult DetailsEvent(Guid id)
        {
            return
                View(_logic.EventSearch(new EventSearchModel
                {
                    EventId = id,
                    PageSize = 1
                }).Single());
        }
        [HttpGet]
        public ActionResult ImageEdit(Guid id)
        {
            return
                View(_logic.SearchImages(new ImageSearchModel
                {
                    PageSize = 1,
                    UserId = ((UserModel)Session["User"]).UserId,
                    ImageId = id
                }).Single());
        }
        [HttpGet]
        public ActionResult ImageDetails(Guid id)
        {
            return
                View(_logic.SearchImages(new ImageSearchModel
                {
                    PageSize = 1,
                    ImageId = id
                }).Single());
        }
        [HttpPost]
        public ActionResult AddCoordinatorToEvent(int userId, Guid eventId)
        {
            try
            {
                var ev = _logic.EventSearch(new EventSearchModel
                {
                    EventId = eventId,
                    PageSize = 1
                }).Single();
                if (ev.Coordinators == null)
                {
                    ev.Coordinators = new List<UserModel>();
                }
                long t;
                ev.Coordinators.Add(_logic.SearchUsers(new UserSearchModel
                {
                    UserId = userId,
                    PageSize = 1
                }, out t).Single());
                _logic.AddOrUpdateEvent(ev, ConfigurationManager.AppSettings["GoogleMapsApiKey"]);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, FriendlyError = "Unable To Add Coordinator" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SearchEvents(EventSearchModel search)
        {
            search.PageSize = 1000;
            if (string.IsNullOrEmpty(search.Code))
            {
                search.Code = "Public";
            }
            return
                View(_logic.EventSearch(search));
        }
        [HttpGet]
        public ActionResult EventAutoComplete(string term)
        {
            var s = _logic.EventSearch(new EventSearchModel
            {
                PageSize = 100,
                EventName = term
            }).Select(x => new { label = x.Name, value = x.EventId }).ToArray();
            return
                Json(
                   s
                , JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult UserAutocomplete(string term)
        {
            long total;
            var s = _logic.SearchUsers(new UserSearchModel
            {
                PageSize = 20,
                NickName = term
            }, out total).Select(z => (z.LastName + ", " + z.FirstName + "("+(z.NickName??"")+")").Replace(":","") + ":" + z.UserId).ToArray();
            s.Union(_logic.SearchUsers(new UserSearchModel
            {
                PageSize = 20,
                LastName = term
            }, out total).Select(z => (z.LastName + ", " + z.FirstName + "(" + (z.NickName ?? "") + ")").Replace(":", "") + ":" + z.UserId).ToArray());
            return
                Json(
                    s
                , JsonRequestBehavior.AllowGet);
        }
      
        [HttpPost]
        [HttpGet]
        public ActionResult GetUsers(Guid? eventId, string lastname, string firstname, string nickname, string username, int pageIndex)
        {
            long total;
            var s = _logic.SearchUsers(new UserSearchModel
            {
                PageSize = 100,
                PageIndex = pageIndex,
                LastName = lastname,
                FirstName = lastname,
                EventId = eventId,
                NickName = nickname
            }, out total);
            ViewBag.Total = total;
            return
                View(s);
        }
      
        [HttpPost]
        public ActionResult RemoveTagFromImage(string tagName, Guid imageId)
        {
            try
            {
                _logic.RemoveTagFromImage(imageId, tagName);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ImageEdit(ImageModel img, string hashTags)
        {
            try
            {
                img.ImageId = Guid.Empty;
                if (Request.Files.Count > 0)
                {
                    var reader = new BinaryReader(Request.Files[0].InputStream);
                    byte[] bits = new byte[Request.Files[0].ContentLength];
                    reader.Read(bits, 0, bits.Length);
                    img.Picture = bits;
                }
                if (hashTags != null && hashTags.Trim() != string.Empty)
                {
                    img.Tags = hashTags.Split('#').Select(x => new TagModel
                    {
                        TagName = x.Trim()
                    }).ToList();
                }
               
                img.ImageId = _logic.AddOrUpdateImage(img, System.IO.File.ReadAllBytes(Server.MapPath("~/Content/GoPickYourselfLogo25.png")));
                _logic.AddImageToUser(img, ((UserModel)Session["User"]));
                return
                    RedirectToAction("SharedPictures");
            }
            catch(Exception ex)
            {
                ViewBag.Error = "Error Updating Image";
                return
                    View(img);
            }
        }
        [HttpPost]
        public ActionResult EditEvent(SharedEventModel eventModel, string hashTags, int EventType)
        {
            return
                UpdateEvent(eventModel, hashTags, EventType);
            //try
            //{
            //    var user = Session["User"] as UserModel;
            //    eventModel.Coordinators = new List<UserModel>();
            //    eventModel.Coordinators.Add(user);
            //    eventModel.EventType = (EventTypeModel)EventType;
            //    if(string.IsNullOrEmpty(eventModel.Code ))
            //    {
            //        eventModel.Code = "Public";
            //    }
            //    if (eventModel.Tags == null)
            //    {
            //        eventModel.Tags = new List<TagModel>();
            //    }
            //    if (!string.IsNullOrEmpty(hashTags))
            //    {
            //        eventModel.Tags.AddRange(CreateTags(hashTags));
            //    }
            //    if(eventModel.Images == null)
            //    {
            //        eventModel.Images = new List<ImageModel>();
            //    }
            //    var list = GetImagesFromFiles(eventModel);
            //    if (list != null)
            //    {
            //        eventModel.Images.AddRange(list);
            //    }
            //    if (eventModel.Images == null)
            //    {
            //        eventModel.Images = new List<ImageModel>();
            //    }
            //    var curCamImg = "cameraupload_";
            //    var index = Request.Form.AllKeys.Where(x => x.Contains(curCamImg));

            //    foreach(var key in index)
            //    {                   
            //        var base64 = Request[key];
            //        if (string.IsNullOrEmpty(base64))
            //        {
            //            continue;
            //        }
            //        base64 = base64.Substring(base64.IndexOf("base64," + 7));
            //        var img = Convert.FromBase64String(base64);
            //        eventModel.Images.Add(new ImageModel
            //        {
            //            Code = Request["Code"] ?? "Public",
            //            IsProfilePicture = false,
            //            Tags = eventModel.Tags,
            //            Picture = img
            //        });
                  
            //    }
            //    if (eventModel.Country == null)
            //    {
            //        eventModel.Country = "USA";
            //    }
            //    var id = _logic.AddOrUpdateEvent(eventModel, ConfigurationManager.AppSettings["GoogleMapsApiKey"]);
            //    return
            //        RedirectToAction("MyEvents");
            //}
            //catch (Exception ex)
            //{
            //    ViewBag.FriendlyError = "An error occurred creating the event";
            //    return
            //        Redirect("~/EventApp/EditEvent?id="+eventModel.EventId);
            //}
        }
        [HttpPost]
        public ActionResult RemoveTagFromEvent(string tagName, Guid eventId)
        {
            try
            {
                _logic.RemoveTagFromEvent(eventId, tagName);
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new {Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        private List<TagModel> CreateTags(string hashTags)
        {
            List<TagModel> m = null;
            if (!string.IsNullOrEmpty(hashTags) && !string.IsNullOrWhiteSpace(hashTags))
            {
                m = hashTags.Split('#').Select(x => new TagModel
                {
                    TagName = x.Trim()
                }).ToList();

            }
            return
                m;
        }
        private List<ImageModel> GetImagesFromFiles(SharedEventModel eventModel)
        {
            BinaryReader reader = null;
            List<ImageModel> images = null;
            if (Request.Files != null && Request.Files.Count > 0)
            {
                images = new List<ImageModel>();
                foreach (string imgF in Request.Files.AllKeys)
                {
                    HttpPostedFileBase img = Request.Files[imgF];
                    byte[] imgs = new byte[img.ContentLength];
                    reader = new BinaryReader(img.InputStream);
                    reader.Read(imgs, 0, imgs.Length);
                    images.Add(new ImageModel
                    {
                        Code = eventModel.Code,
                        IsProfilePicture = false,
                        Picture = imgs,
                        Tags = eventModel.Tags
                    });
                }
            }
            eventModel.Images = images;
            return images;
        }
        //private static void BindPicture(ImageModel pic)
        //{
        //    try
        //    {
        //        //var runner = new ImageRunner(ConfigurationManager.AppSettings["FaceSDKKey"],
        //        //ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        //        //runner.RunImageComparer(pic);
        //    }
        //    catch(Exception ex)
        //    {

        //    }
        //}
        [HttpGet]
        public ActionResult VideoToImageConverter()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult AddBase64Image(string imgStr)
        {
            if (imgStr == null)
            {
                return
                    Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
            try {
                imgStr = imgStr.Substring(imgStr.IndexOf("base64,") + 7);
                var imgBits = Convert.FromBase64String(imgStr);
                var id = _logic.AddOrUpdateImage(new ImageModel
                {
                    Code = "Public",
                    IsProfilePicture = false,
                    Picture = imgBits,
                    PeopleInPicture = new List<UserModel>
                {
                    (UserModel)Session["User"]
                }
                }, System.IO.File.ReadAllBytes(Server.MapPath("~/Content/GoPickYourselfLogo25.png")));
                var imgModel = _logic.SearchImages(new ImageSearchModel
                {
                    PageSize = 1,
                    ImageId = id
                }).Single();
                _logic.AddImageToUser(imgModel, ((UserModel)Session["User"]));
                //ThreadPool.QueueUserWorkItem(delegate
                //{
                //    BindPicture(imgModel);
                //});
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult FacebookProfilePicUpload(string imgUrl)
        {
            try
            {
                var client = new WebClient();
                var bits = client.DownloadData(imgUrl);
                var id = _logic.AddOrUpdateImage(new ImageModel
                {
                   IsProfilePicture = true,
                   Event = null,
                   Code = "Public",
                   PeopleInPicture = new List<UserModel>
                   {
                       ((UserModel)Session["User"])
                   } ,
                   Picture = bits
                }, System.IO.File.ReadAllBytes(Server.MapPath("~/Content/GoPickYourselfLogo25.png")));
                var ev = _logic.SearchImages(new ImageSearchModel
                {
                    ImageId = id,
                    PageSize = 1
                }).Single();
                _logic.AddImageToUser(ev, ((UserModel)Session["User"]));
                //ThreadPool.QueueUserWorkItem(delegate
                //{
                //    BindPicture(ev);
                //});
                return
                    Json(new {Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult FacebookImport()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult FacebookAddEvent(bool isOwner, string description, string name, DateTime eventDate)
        {
            try
            {
                if (isOwner)
                {
                    _logic.AddOrUpdateEvent(new SharedEventModel
                    {
                        Name = name,
                        Description = description,
                        CreatedDate = DateTime.UtcNow,
                        Coordinators = new List<UserModel>
                        {
                            ((UserModel)Session["User"])
                        },
                        EventDate = eventDate,
                        EventType = EventTypeModel.PUBLIC
                    }, ConfigurationManager.AppSettings["GoogleMapsApiKey"]);
                }
                return
                    Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
            
        }
        [HttpPost]
        public ActionResult CreateEvent(SharedEventModel eventModel, string hashTags, int EventType)
        {
            return UpdateEvent(eventModel, hashTags, EventType);
        }
        private ActionResult UpdateEvent(SharedEventModel eventModel, string hashTags, int EventType)
        {
            try
            {
                if (string.IsNullOrEmpty(eventModel.Code))
                {
                    eventModel.Code = "Public";
                }
                var curCamImg = "cameraupload_";



                var user = Session["User"] as UserModel;
                eventModel.Coordinators = new List<UserModel>();
                eventModel.Coordinators.Add(user);
                eventModel.EventType = (EventTypeModel)EventType;
                eventModel.Tags = CreateTags(hashTags);

                if (string.IsNullOrEmpty(eventModel.Country))
                {
                    eventModel.Country = "USA";
                }

                eventModel.Images = GetImagesFromFiles(eventModel);
                if (eventModel.Images == null)
                {
                    eventModel.Images = new List<ImageModel>();
                }
                var index = Request.Form.AllKeys.Where(x => x.Contains(curCamImg));

                foreach (var key in index)
                {
                    var base64 = Request[key];
                    if (string.IsNullOrEmpty(base64))
                    {
                        continue;
                    }
                    base64 = base64.Substring(base64.IndexOf("base64,") + 7);
                    var img = Convert.FromBase64String(base64);
                    var imgMod = new ImageModel
                    {
                        Code = Request["Code"] ?? "Public",
                        IsProfilePicture = false,
                        Tags = eventModel.Tags,
                        Picture = img                        
                    };
                    imgMod.ImageId = Guid.Empty;
                    imgMod.ImageId = _logic.AddOrUpdateImage(imgMod, System.IO.File.ReadAllBytes(Server.MapPath("~/Content/GoPickYourselfLogo25.png")));
                    eventModel.Images.Add(imgMod);
                    _logic.AddImageToUser(imgMod, ((UserModel)Session["User"]));
                    //ThreadPool.QueueUserWorkItem(delegate
                    //{ 
                    //   BindPicture(imgMod);
                    //});
                }
                var id = _logic.AddOrUpdateEvent(eventModel, ConfigurationManager.AppSettings["GoogleMapsApiKey"]);
               
                return
                    Redirect("~/EventApp/MyEvents");
            }
            catch (Exception ex)
            {
               
                ViewBag.Error = "An error occurred creating the event";
                return
                    View(eventModel);
            }
        }
        [HttpGet]
        public ActionResult AddImage()
        {
            return
                View();
        }
        [HttpGet]
        public ActionResult SharedPictures()
        {
            return
                View(_logic.SearchImages(new ImageSearchModel
                {
                    PageSize = int.MaxValue,
                   
                    UserId = ((UserModel)Session["User"]).UserId
                }));
        }
        
        [HttpPost]
        public ActionResult MyProfile(UserModel user)
        {
            try
            {
                user.UserLevel = UserLevelModel.USER;

                user.UserId = _logic.RegisterOrUpdateUser(user);

                if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
                {
                    var buf = new byte[Request.Files["profilePic"].ContentLength];
                    var reader = new System.IO.BinaryReader(Request.Files["profilePic"].InputStream);
                    reader.Read(buf, 0, buf.Length);
                    var image = new ImageModel
                    {
                        IsProfilePicture = true,
                        Code = "Public",
                        Picture = buf,
                        Tags = new List<TagModel>
                    {
                        new TagModel
                        {
                            TagName = user.NickName
                        }
                    }
                    };
                    image.ImageId = _logic.AddOrUpdateImage(image, System.IO.File.ReadAllBytes(Server.MapPath("~/Content/GoPickYourselfLogo25.png")));
                    _logic.AddImageToUser(image, user);
                }
                return
                    RedirectToAction("Dashboard");
            }
            catch (Exception ex)
            {
                ViewBag.FriendlyError = ex.ToString() + "Error Updating Profile";
                return
                    View(user);
            }
        }
        [HttpGet]
        public ActionResult Search()
        {
            return
                View();
        }
        public ActionResult SearchImages(string hashtags, string imagecode)
        {
            var models = new List<ImageModel>();
            if(!string.IsNullOrEmpty(hashtags))
            {
                models.AddRange(_logic.SearchImages(new ImageSearchModel
                {
                    Tags = hashtags.Split('#').Where(x => !string.IsNullOrEmpty(x)).ToList(),
                    PageSize = int.MaxValue/2
                }));
            }
            if(!string.IsNullOrEmpty(imagecode))
            {
                if (models.Count > 0)
                {
                    models = models.Intersect(_logic.SearchImages(new ImageSearchModel
                    {
                        PageSize = int.MaxValue,
                        Code = imagecode
                    })).ToList();
                }
                else
                {
                    models = _logic.SearchImages(new ImageSearchModel
                    {
                        PageSize = int.MaxValue,
                        Code = imagecode
                    });
                }
            }
            return View(models);
        }
        [HttpGet] 
        public ActionResult MyProfile()
        {
            long total;
            var user = _logic.SearchUsers(new UserSearchModel
            {
                PageSize = 1,
                UserId = ((UserModel)Session["User"]).UserId
            }, out total).Single();
            ViewBag.Images = _logic.SearchImages(new ImageSearchModel
            {
                UserId = user.UserId,
                IsProfilePic = true,
                PageSize = int.MaxValue
            });
            return
                View(user);
        }
        [HttpGet]
        public ActionResult Dashboard()
        {
            return
                View();
        }

    }
}