﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.Events.BusinessLogic;
using WSDLLC.Events.Core.Contracts;
using WSDLLC.Events.Domain.Models;
using WSDLLC.Events.WebApp.Helpers;

namespace WSDLLC.Events.WebApp.Controllers
{
    [SecureContextHeader]
    public class HomeController : Controller
    {
        private readonly IAppBusinessLogic _logic;
        public HomeController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        [HttpGet]
        public ActionResult CheckAnouncements()
        {
            try
            {
                var currentAnouncement = _logic.SearchAnouncement(new AnouncementSearch
                {
                    CurrentDate = DateTime.UtcNow,
                    PageSize = 1
                }).SingleOrDefault();
                var text = currentAnouncement == null ? null : currentAnouncement.Text;
                return
                    Json(new { Success = true,
                        Message = text
                    }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Error = ex.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpGet]
        public ActionResult IndexIOSStart()
        {
            return
                View();
        }
        public ActionResult GenerateCode(string username)
        {
            try
            {
                _logic.GenerateCode(username, username, ConfigurationManager.AppSettings["EmailUsername"],
                    ConfigurationManager.AppSettings["EmailPassword"], ConfigurationManager.AppSettings["AccountID"],
                    ConfigurationManager.AppSettings["SMSProviderUrl"]);
                return
                    Json(new { Success = true}, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new {Error = ex.ToString(), Success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ChangePassword(string code, string newPassword, string username)
        {
            try
            {
                _logic.ValidateCode(username, code, newPassword);
                return
                    Json(new {Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Go Pick Yourself is a Wooden Software Development LLC Application made inconjunction with STMT Inc.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Call Stephen Georgoules at +1(847)571-8353.";

            return View();
        }
        [HttpGet]
        public ActionResult CreateProfile()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult CreateProfile(UserModel user)
        {
            try
            {
               
                  
                user.UserLevel = UserLevelModel.USER;
                
                user.UserId = _logic.RegisterOrUpdateUser(user);
              
                if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
                {
                    var buf = new byte[Request.Files["profilePic"].ContentLength];
                    var reader = new System.IO.BinaryReader(Request.Files["profilePic"].InputStream);
                    reader.Read(buf, 0, buf.Length);
                    var image = new ImageModel
                    {
                        IsProfilePicture = true,
                        Code = "Public",
                        Picture = buf,
                        Tags = new List<TagModel>
                    {
                        new TagModel
                        {
                            TagName = user.NickName
                        }
                    }
                    };
                    image.ImageId = _logic.AddOrUpdateImage(image, System.IO.File.ReadAllBytes(Server.MapPath("~/Content/GoPickYourselfLogo25.png")));
                    _logic.AddImageToUser(image, user);
                }
                return
                    Redirect("~/Home/Index");
            }
            catch (Exception ex)
            {
                ViewBag.FriendlyError = "Error Making Profile";
                return
                    View(user);
            }
        }
        public ActionResult ResetPasswordWithCode()
        {
            return
                View();
        }
        public ActionResult ResetPasswordWithoutCode()
        {
            return
                View();
        }
        public ActionResult TermsAndConditions()
        {
            return View();
        }
        public ActionResult Login(string username, string pin)
        {
            Session["User"] = _logic.ValidateUser(username, username, username, pin);
            if(Session["User"] == null)
            {
                return
                    Json(new {Success = false, Error = "Invalid Credentials" }, JsonRequestBehavior.AllowGet);
            }
            return
                Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}