﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.Events.BusinessLogic;
using WSDLLC.Events.Core.Contracts;
using WSDLLC.Events.Domain.Models;
using WSDLLC.Events.WebApp.Helpers;

namespace WSDLLC.Events.WebApp.Controllers
{
    [SecureContextHeader]
    [AuthorizationNeededHeader]
    public class ManagementController : Controller
    {
        private readonly IAppBusinessLogic _logic;
        public ManagementController()
        {
            _logic = new AppBusinessLogic(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        }
        [HttpGet]
        public ActionResult AnouncementManagement()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult SearchAnouncements(AnouncementSearch search)
        {
            search.PageSize = int.MaxValue;
            search.PageIndex = 0;
            return
                View(_logic.SearchAnouncement(search));
        }
        // GET: Management
        public ActionResult ManagementPanel()
        {
            return View();
        }
        [HttpGet]
        public ActionResult EditAnouncement(int id)
        {
            return
                View(_logic.SearchAnouncement(new AnouncementSearch
                {
                    Id = id,
                    PageSize = 1
                }).Single());
        }
        [HttpPost]
        public ActionResult EditAnouncement(AnouncementModel anouncement)
        {
            try
            {
                _logic.AddOrUpdateAnouncement(anouncement);
                return
                    RedirectToAction("AnouncementManagement");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(anouncement);
            }
        }
        [HttpGet]
        public ActionResult AddAnouncement()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult AddAnouncement(AnouncementModel anouncement)
        {
            try
            {
                _logic.AddOrUpdateAnouncement(anouncement);
                return
                    RedirectToAction("AnouncementManagement");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return
                    View(anouncement);
            }
        }
        [HttpGet]
        public ActionResult CreateUser()
        {
            return
                View();
        }
        [HttpPost]
        public ActionResult CreateUser(UserModel user, int userLevel)
        {
            try
            {
                user.UserLevel = (UserLevelModel)userLevel;
                _logic.RegisterOrUpdateUser(user);
                return
                    RedirectToAction("Users");
            }
            catch (Exception ex)
            {
                return
                    View(user);
            }
        }
        [HttpGet]
        public ActionResult EditUser(long id)
        {
            long total;
            return
                View(_logic.SearchUsers(new UserSearchModel
                {
                    PageSize = 1,
                    UserId = id
                }, out total).Single());
        }
        [HttpPost]
        public ActionResult EditUser(UserModel user, int userLevel)
        {
            try
            {
                user.UserLevel = (UserLevelModel)userLevel;
                _logic.RegisterOrUpdateUser(user);
                return
                    RedirectToAction("Users");
            }
            catch (Exception ex)
            {
                ViewBag.FriendlyError = "Unable To Update User";
                return
                    View(user);
            }
        }
        [HttpGet]
        public ActionResult Users()
        {
            long total;
            var s = _logic.SearchUsers(new UserSearchModel
            {
                PageSize = 100
            }, out total);
            ViewBag.Total = total;
            return
                View(s);
        }
    }
}