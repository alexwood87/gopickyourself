﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSDLLC.Events.Domain.Models;
namespace WSDLLC.Events.WebApp.Helpers
{
    public class AuthorizationNeededHeader : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var s = filterContext.HttpContext.Session["User"] as UserModel;
            if (s == null)
            {
                filterContext.HttpContext.Response.Redirect("~/Home/Index");
            }
            if(filterContext.HttpContext.Request.Path.Contains("Management"))
            {
                if(!(s.UserLevel == UserLevelModel.SUPERADMIN ||s.UserLevel == UserLevelModel.ADMIN))
                {
                    filterContext.HttpContext.Response.Redirect("~/Home/Index");
                }
            }
        }
    }
}