﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace WSDLLC.Events.WebApp.Helpers
{
    public static class HtmlExtensions
    {
        public static string ImageControlFromBytes(byte[] bits, string name, string alt)
        {
            var bit64 = Convert.ToBase64String(bits);
            return
                string.Format("<img src='data:image/png;base64,{0}' alt='{2}' width='400' height='400' name='{1}' id='{1}' />",
                bit64.Replace("\u003d", "="), name, alt);
        } 
        public static List<SelectListItem> EventTypesList(int typeChosen)
        {
            return
                new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Value = "1",
                        Text = "Public",
                        Selected = typeChosen == 1 || typeChosen == 0
                    },
                    new SelectListItem
                    {
                        Value = "2",
                        Text = "Friends Only",
                        Selected = typeChosen == 2
                    },
                    new SelectListItem
                    {
                        Value = "3",
                        Text = "Private",
                        Selected = typeChosen == 3
                    }
                };
        }

        public static string GoogleMapIcon(decimal lat, decimal lon)
        {
            return
                string.Format("<a href='http://maps.apple.com/?q={0},{1}'><img src='/Content/google_maps.png' alt='Map Link'/></a>",
                    lat.ToString("G29"), lon.ToString("G29"));

        }
    }
}