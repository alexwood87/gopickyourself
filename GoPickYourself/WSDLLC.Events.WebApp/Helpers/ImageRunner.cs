﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSDLLC.Events.BusinessLogic;
using WSDLLC.Events.Core.Contracts;
using WSDLLC.Events.Domain.Models;

namespace WSDLLC.Events.WebApp.Helpers
{
    public class ImageRunner
    {
        private readonly IAppBusinessLogic _logic;
        public ImageRunner(string apiKey, string connString)
        {
            _logic = new AppBusinessLogic(connString);

            if (Luxand.FSDK.ActivateLibrary(apiKey) != Luxand.FSDK.FSDKE_OK)
            {
                throw new ArgumentException("Unable to Activate Library");
            }
            Luxand.FSDK.SetFaceDetectionParameters(true, true, 500);
        }
        public void RunImageComparer(ImageModel img)
        {
            long total;
            var users = _logic.SearchUsers(new Domain.Models.UserSearchModel
            {
                PageSize = int.MaxValue                
            }, out total);
            var tempImg = img.Picture;

            foreach (var u in users)
            {
                if(u.ProfilePictures == null)
                {
                    continue;
                }
                foreach (var im in u.ProfilePictures)
                {
                    float accurracy = 0.75f;
                    var imPic = im.Picture;

                    Luxand.FSDK.MatchFaces(ref imPic, ref tempImg,
                        ref accurracy);
                    if (accurracy >= 0.75f)
                    {
                        _logic.AddImageToUser(img, u);
                    }
                }
            }
        }
    }
}
