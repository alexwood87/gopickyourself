﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WSDLLC.Events.WebApp.Helpers
{
    public class SecureContextHeader : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var path = filterContext.HttpContext.Request.Url.AbsoluteUri;
            if (!path.ToLower().StartsWith("https") && path.ToLower().Contains("gopickyourself"))
            {
                path = "https" + path.Substring(4);
                filterContext.HttpContext.Response.Redirect(path);
            }
        }
    }
}