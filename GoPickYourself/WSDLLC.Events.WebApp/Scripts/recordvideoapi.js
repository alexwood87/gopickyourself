﻿function hasGetUserMedia() {
    return (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia
               || navigator.msGetUserMedia);
}

var constraints;
if (getBrowser() == "Chrome") {
    constraints = { "audio": true, "video": { "mandatory": { "minWidth": 640, "maxWidth": 640, "minHeight": 480, "maxHeight": 480 }, "optional": [] } };//Chrome
} else if (getBrowser() == "Firefox") {
    constraints = { audio: true, video: { width: { min: 640, ideal: 640, max: 640 }, height: { min: 480, ideal: 480, max: 480 } } }; //Firefox
}
var mediaRecorder, videoElement;
var invitationCode, thumbnails=0;
var chunks = [];
function startRecording(stream) {
    console.log('Starting...');
    if (typeof MediaRecorder.isTypeSupported == 'function') {
        /*
			MediaRecorder.isTypeSupported is a function announced in https://developers.google.com/web/updates/2016/01/mediarecorder and later introduced in the MediaRecorder API spec http://www.w3.org/TR/mediastream-recording/
		*/
        var options;
        if (MediaRecorder.isTypeSupported('video/webm;codecs=h264')) {
            options = { mimeType: 'video/webm;codecs=h264' };
        } else if (MediaRecorder.isTypeSupported('video/webm;codecs=vp9')) {
            options = { mimeType: 'video/webm;codecs=vp9' };
        } else if (MediaRecorder.isTypeSupported('video/webm;codecs=vp8')) {
            options = { mimeType: 'video/webm;codecs=vp8' };
        }
        console.log('Using ' + options.mimeType);
        mediaRecorder = new MediaRecorder(stream, options);
    } else {
        console.log('Using default codecs for browser');
        mediaRecorder = new MediaRecorder(stream);
    }


    mediaRecorder.start();

    var url = window.URL || window.webkitURL;
    videoElement.src = url ? url.createObjectURL(stream) : stream;
    videoElement.play();

    mediaRecorder.ondataavailable = function (e) {
        
        console.log(e.data);
       
       
        chunks.push(e.data);
       
    };

    mediaRecorder.onerror = function (e) {
        //log('Error: ' + e);
        console.log('Error: ', e);
    };


    mediaRecorder.onstart = function () {
        chunks = [];
        console.log('Started, state = ' + mediaRecorder.state);
    };

    mediaRecorder.onstop = function () {
        console.log('Stopped, state = ' + mediaRecorder.state);

        var blob = new Blob(chunks, { type: "video/webm" });
        generateThumbnail(thumbnails++);
        document.getElementById(_videoId).src = null;
        //var blob = new Blob(chunks, { type: "video/webm" });
        var reader = new window.FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = function () {
            base64data = reader.result;
            console.log(base64data);
            if (base64data.length >= 2000000000) {
                alert('Movie too long!');
                return;
            }
           
        }
        
    };

    mediaRecorder.onwarning = function (e) {
        console.log('Warning: ' + e);
    };
}

function download() {
    var blob = new Blob(chunks, {
        type: 'video/webm'
    });
    var url = URL.createObjectURL(blob);
    var a = document.createElement('a');
    document.body.appendChild(a);
    a.style = 'display: none';
    a.href = url;
    a.download = 'myVideo.webm';
    a.click();
    window.URL.revokeObjectURL(url);
}

function ErrorRejectCallback(e) {
    console.log('Rejected Allow Use Camera!', e);
    alert('You Must Allow Camera Use!');
}
var streamStr = "";
var _videoId;
function LoadCamera(videoId) {
    var browser = getBrowser();
    if (browser !== "Chrome" && browser !== "Firefox") {
        alert('Browser Not Supported - use Firefox or Chrome');
    }
    _videoId = videoId;
    if (!hasGetUserMedia()) {
        alert('Browser Does Not Support Video Recording!');
        return;
    }
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
    // Not showing vendor prefixes.
    navigator.getUserMedia({ video: true },
        function (localMediaStream) {
            videoElement = document.getElementById(_videoId);
            if(!("url" in window) && ("webkitURL" in window)) 
            {
                window.URL = window.webkitURL;
            }
            streamStr = window.URL.createObjectURL(localMediaStream);
            startRecording(localMediaStream);
            
        }, ErrorRejectCallback);
}

function StopRecording()
{
    if(mediaRecorder)
    {
        mediaRecorder.stop();
        $("#"+_videoId)[0].pause();
        
    }
}
