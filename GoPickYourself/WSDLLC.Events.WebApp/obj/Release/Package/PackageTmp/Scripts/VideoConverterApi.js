﻿var video;
function UploadImages() {
    var imgs = $('#thumbnailContainer').children();
    for (var i in imgs) {
        var img = imgs[i];
        
        if (!img || !img.src || img.src == undefined)
        {
            continue;
        }
        $.ajax({
            url: '/EventApp/AddBase64Image',
            method: 'POST',
            data: {
                imgStr: img.src
            },
            success: function (data) {
                alert('Image Uploaded');
            },
            error: function (a, b, c) {
                alert(c);
            }
        });
    }
}
function generateThumbnail(i) {
    var thecanvas = document.getElementById('preview');
    if (!thecanvas)
    {
        thecanvas = document.getElementById('previewCanvas');
    }
    //alert(thecanvas);
    video = document.getElementById('player');
    //generate thumbnail URL data

    var context = thecanvas.getContext('2d');
    context.drawImage(video, 0, 0);
    var dataURL = thecanvas.toDataURL('image/png');
    
    //create img
    var img = document.createElement('img');
    img.setAttribute('src', dataURL);
    img.id = 'imgcameraimg_' + i;
    img.name = 'imgcameraimg_' + i;
    var hf = document.createElement('input');
    hf.type = 'hidden';
    hf.id = 'cameraupload_' + i;
    hf.name = 'cameraupload_' + i;
    hf.value = dataURL;
    //append img in container div
    document.getElementById('thumbnailContainer').appendChild(img);
    document.getElementById('thumbnailContainer').appendChild(hf);
}
// this must be global (or bound to a unique object/instance) so it
// is available for all event handlers
var i = 0;
$(document).ready(function(){
    video= document.getElementById('player');

    video.addEventListener('loadeddata', function () {

        video.currentTime = i;

    }, false);


    video.addEventListener('seeked', function () {

        // now video has seeked and current frames will show
        // at the time as we expect
        generateThumbnail(i);

        // when frame is captured, increase
        i += 5;

        // if we are not passed end, seek to next interval
        if (i <= video.duration) {
            // this will trigger another seeked event
            video.currentTime = i;

        } else {
            // DONE!, next action
        }

    }, false);
    
});