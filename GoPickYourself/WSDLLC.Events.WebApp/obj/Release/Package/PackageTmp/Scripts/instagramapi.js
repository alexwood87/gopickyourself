﻿$(document).ready(function () {
    $('#btnImportInstagram').click(function () {
        var myUserId = $('#txtInstagramUserId').val();
        var feed = new Instafeed({
            get: 'user',
            clientId: '15434d7593b44c2f8ff9fd071803d3d7',
            userId: myUserId,
            success: function (image) {
                UploadMyPicture(image);
            }
        });
        feed.run();
    });
});