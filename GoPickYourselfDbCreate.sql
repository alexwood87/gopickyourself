USE [GoPickYourself]
GO
/****** Object:  Table [dbo].[Anouncements]    Script Date: 3/21/2017 12:31:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Anouncements](
	[AnouncementId] [int] IDENTITY(1,1) NOT NULL,
	[Anouncement] [nvarchar](max) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AnouncementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[CodeReset]    Script Date: 3/21/2017 12:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CodeReset](
	[CodeResult] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](200) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CodeResult] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[EventsToUsers]    Script Date: 3/21/2017 12:31:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventsToUsers](
	[EventToUsersId] [uniqueidentifier] NOT NULL,
	[SharedEventId] [uniqueidentifier] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[IsCoordinator] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EventToUsersId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[EventTypes]    Script Date: 3/21/2017 12:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventTypes](
	[EventTypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EventTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Images]    Script Date: 3/21/2017 12:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Images](
	[ImageId] [uniqueidentifier] NOT NULL,
	[Picture] [image] NOT NULL,
	[Code] [nvarchar](100) NOT NULL,
	[SharedEventId] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[ImagesToUsers]    Script Date: 3/21/2017 12:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImagesToUsers](
	[ImagesToUsersId] [uniqueidentifier] NOT NULL,
	[ImageId] [uniqueidentifier] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[IsProfilePicture] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ImagesToUsersId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[SharedEvents]    Script Date: 3/21/2017 12:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SharedEvents](
	[SharedEventId] [uniqueidentifier] NOT NULL,
	[EventName] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[EventDate] [datetime] NOT NULL,
	[EventTypeId] [int] NULL,
	[Code] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[City] [nvarchar](100) NULL,
	[StateOrProvince] [nvarchar](100) NULL,
	[PostalCode] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[Latitude] [decimal](29, 20) NULL,
	[Longitude] [decimal](29, 20) NULL,
	[EventEndDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[SharedEventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Tags]    Script Date: 3/21/2017 12:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tags](
	[TagId] [bigint] IDENTITY(1,1) NOT NULL,
	[TagName] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[TagsToEvents]    Script Date: 3/21/2017 12:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TagsToEvents](
	[TagsToEventsId] [uniqueidentifier] NOT NULL,
	[TagId] [bigint] NOT NULL,
	[SharedEventId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TagsToEventsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[TagsToImages]    Script Date: 3/21/2017 12:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TagsToImages](
	[TagsToImagesId] [uniqueidentifier] NOT NULL,
	[TagId] [bigint] NOT NULL,
	[ImageId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TagsToImagesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Users]    Script Date: 3/21/2017 12:31:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[NickName] [nvarchar](100) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NULL,
	[AuthorizationLevel] [int] NOT NULL,
	[PhoneNumber] [nvarchar](100) NULL,
	[Email] [nvarchar](1000) NULL,
	[Pin] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
ALTER TABLE [dbo].[CodeReset] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[EventsToUsers] ADD  DEFAULT ((0)) FOR [IsCoordinator]
GO
ALTER TABLE [dbo].[Images] ADD  DEFAULT (newid()) FOR [ImageId]
GO
ALTER TABLE [dbo].[Images] ADD  DEFAULT (N'Public') FOR [Code]
GO
ALTER TABLE [dbo].[ImagesToUsers] ADD  DEFAULT (newid()) FOR [ImagesToUsersId]
GO
ALTER TABLE [dbo].[ImagesToUsers] ADD  DEFAULT ((0)) FOR [IsProfilePicture]
GO
ALTER TABLE [dbo].[SharedEvents] ADD  DEFAULT (newid()) FOR [SharedEventId]
GO
ALTER TABLE [dbo].[SharedEvents] ADD  DEFAULT (N'Public') FOR [Code]
GO
ALTER TABLE [dbo].[SharedEvents] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TagsToEvents] ADD  DEFAULT (newid()) FOR [TagsToEventsId]
GO
ALTER TABLE [dbo].[TagsToImages] ADD  DEFAULT (newid()) FOR [TagsToImagesId]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT ((3)) FOR [AuthorizationLevel]
GO
ALTER TABLE [dbo].[CodeReset]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[EventsToUsers]  WITH CHECK ADD FOREIGN KEY([SharedEventId])
REFERENCES [dbo].[SharedEvents] ([SharedEventId])
GO
ALTER TABLE [dbo].[EventsToUsers]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Images]  WITH CHECK ADD FOREIGN KEY([SharedEventId])
REFERENCES [dbo].[SharedEvents] ([SharedEventId])
GO
ALTER TABLE [dbo].[ImagesToUsers]  WITH CHECK ADD FOREIGN KEY([ImageId])
REFERENCES [dbo].[Images] ([ImageId])
GO
ALTER TABLE [dbo].[ImagesToUsers]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[SharedEvents]  WITH CHECK ADD FOREIGN KEY([EventTypeId])
REFERENCES [dbo].[EventTypes] ([EventTypeId])
GO
ALTER TABLE [dbo].[TagsToEvents]  WITH CHECK ADD FOREIGN KEY([SharedEventId])
REFERENCES [dbo].[SharedEvents] ([SharedEventId])
GO
ALTER TABLE [dbo].[TagsToEvents]  WITH CHECK ADD FOREIGN KEY([TagId])
REFERENCES [dbo].[Tags] ([TagId])
GO
ALTER TABLE [dbo].[TagsToImages]  WITH CHECK ADD FOREIGN KEY([ImageId])
REFERENCES [dbo].[Images] ([ImageId])
GO
ALTER TABLE [dbo].[TagsToImages]  WITH CHECK ADD FOREIGN KEY([TagId])
REFERENCES [dbo].[Tags] ([TagId])
GO
